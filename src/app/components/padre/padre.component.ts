import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PADREComponent {
  @Output() eventF = new EventEmitter<string[]>();

  //numRomani: string[] = ["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"];
  //numDRom: string[] = [];

  numRomani: string[] = ["I", "III", "V", "VII", "IX"];
  clickButtonF(){
    /*this.numRomani.forEach((num, i) => {
      if( i % 2 == 0){
        this.numDRom.push(num);
      }
    })*/
    this.eventF.emit(this.numRomani);
  }
}
